import { React, useState, useEffect } from 'react'
import { Form, Button, Input } from 'semantic-ui-react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

function update () {
  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [city, setCityname] = useState('')
  const [emailid, setEmailid] = useState('')
  const [ID, setID] = useState(null)
  const navigate = useNavigate()

  const Api = axios.create({
    baseURL: 'https://6364b00c8a3337d9a2fc18f2.mockapi.io/cruddata/'
  })

  const updateDatatoapi = () => {
    console.log(firstname)
    console.log(lastname)
    console.log(city)
    console.log(emailid)
    console.log(ID)
    Api.put(`${ID}`, {
      firstname,
      lastname,
      city,
      emailid,
      ID
    }).then((res) => {
      setFirstname(res.firstname)
      setLastname(res.lastname)
      setCityname(res.city)
      setEmailid(res.emailid)
      setID(res.ID)
    }).then(() => {
      navigate('/read')
    }).catch((error) => {
      console.log(error)
    })
  }

  useEffect(() => {
    setFirstname(localStorage.getItem('firstName'))
    setLastname(localStorage.getItem('lastName'))
    setCityname(localStorage.getItem('city'))
    setEmailid(localStorage.getItem('emailid'))
    setID(localStorage.getItem('ID'))
  }, [])

  return (
    <div>

    <Form>
      <Form.Group widths='equal'>
        <Form.Field
          id='form-input-control-first-name'
          control={Input}
          value={firstname}
          name = 'fname'
          label='First name'
          placeholder='First name'
          onChange ={(e) => setFirstname(e.target.value)}
        />
        <Form.Field
          id='form-input-control-last-name'
          control={Input}
          value={lastname}
          name='lname'
          label='Last name'
          placeholder='Last name'
          onChange ={(e) => setLastname(e.target.value)}
        />

      </Form.Group>
      <Form.Field
        id='form-textarea-control-opinion'
        control={Input}
        name = 'city'
        value = {city}
        label='city'
        placeholder='Enter City Name'
        onChange ={(e) => setCityname(e.target.value)}
      />
      <Form.Field
        id='form-input-control-error-email'
        control={Input}
        label='Email'
        name = 'email'
        value = {emailid}
        placeholder='joe@schmoe.com'
        error={{
          content: 'Please enter a valid email address',
          pointing: 'below'
        }}
        onChange ={(e) => setEmailid(e.target.value)}
      />
      <div>
      <Button positive
      onClick={updateDatatoapi}
      >
          Update
          </Button>

      </div>
    </Form>

  </div>
  )
}

export default update
