import React, { useEffect, useState } from 'react'
import { Table, Button } from 'semantic-ui-react'
import axios from 'axios'
// import { useNavigate, Link } from 'react-router-dom'
import { Link } from 'react-router-dom'

export default function Read () {
  const [apiData, setApiData] = useState([])
  // const navigate = useNavigate()

  const Api = axios.create({
    baseURL: 'https://6364b00c8a3337d9a2fc18f2.mockapi.io/cruddata/'
  })

  useEffect(() => {
    Api.get('')
      .then((getData) => {
        setApiData(getData.data)
      })
  }, [])

  // const gotoUpdate = () => {
  //   navigate('/update')
  // }

  const setData = (id, firstName, lastName, city, emailid) => {
    localStorage.setItem('ID', id)
    localStorage.setItem('firstName', firstName)
    localStorage.setItem('lastName', lastName)
    localStorage.setItem('city', city)
    localStorage.setItem('emailid', emailid)
  }

  const getData = () => {
    Api.get('')
      .then((getData) => {
        setApiData(getData.data)
      })
  }

  const onDelete = (id) => {
    console.log(id)
    Api.delete(`${id}`)
      .then((res) =>
        console.log('delete data', res.data),
      alert('axios delete')
      )
      .then(() => {
        getData()
      })
      .catch((error) => {
        console.log(error)
      })
  }

  return (
        <div>
           <Link to='/'>
           <Button color="blue">Go Back</Button>
           </Link>

            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>ID</Table.HeaderCell>
                        <Table.HeaderCell>First Name</Table.HeaderCell>
                        <Table.HeaderCell>Last Name</Table.HeaderCell>
                        <Table.HeaderCell>City</Table.HeaderCell>
                        <Table.HeaderCell>Email</Table.HeaderCell>
                        <Table.HeaderCell>Update</Table.HeaderCell>
                        <Table.HeaderCell>Delete</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {apiData.map((data, index) => {
                      return (
                            <Table.Row key={index}>
                                <Table.Cell>{data.id}</Table.Cell>
                                <Table.Cell>{data.firstname}</Table.Cell>
                                <Table.Cell>{data.lastname}</Table.Cell>
                                <Table.Cell>{data.city}</Table.Cell>
                                <Table.Cell>{data.emailid}</Table.Cell>
                                <Table.Cell>
                                  <Link to='/update'>
                                <Button color="black"
                                onClick={() => setData(data.id, data.firstname, data.lastname, data.city, data.emailid)}
                                 >Update</Button>
                                 </Link>

                                </Table.Cell>
                                <Table.Cell>
                                <Link to='/read'>
                                    <Button color="red"
                                    onClick={() => onDelete(data.id)}
                                    >Delete</Button>
                                    </Link>
                                </Table.Cell>
                            </Table.Row>
                      )
                    })}

                </Table.Body>
            </Table>
        </div>
  )
}
