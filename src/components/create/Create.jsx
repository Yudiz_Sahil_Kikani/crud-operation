import { React, useState } from 'react'
import { Form, Input, Button } from 'semantic-ui-react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

function Create () {
  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [city, setCityname] = useState('')
  const [emailid, setEmailid] = useState('')
  const navigate = useNavigate()

  const Api = axios.create({
    baseURL: 'https://6364b00c8a3337d9a2fc18f2.mockapi.io/cruddata/'
  })

  // console.log(firstname)
  // console.log(lastname)
  // console.log(city)
  // console.log(emailid)

  const addDatatoapi = () => {
    Api.post('', {
      firstname,
      lastname,
      city,
      emailid
    }).then(() => {
      alert('axios post')
    })
      .then(() => {
        navigate('/read')
      })
  }

  return (
<div className='container'>

  <Form>
    <Form.Group widths='equal'>
      <Form.Field
        id='form-input-control-first-name'
        required
        control={Input}
        name = 'fname'
        label='First name'
        placeholder='First name'
        onChange ={(e) => setFirstname(e.target.value)}
      />
      <Form.Field
        id='form-input-control-last-name'
        required
        control={Input}
        name='lname'
        label='Last name'
        placeholder='Last name'
        onChange ={(e) => setLastname(e.target.value)}
      />

    </Form.Group>
    <Form.Field
      id='form-textarea-control-opinion'
      required
      control={Input}
      name = 'city'
      label='city'
      placeholder='Enter City Name'
      onChange ={(e) => setCityname(e.target.value)}
    />
    <Form.Field
      id='form-input-control-error-email'
      required
      control={Input}
      label='Email'
      name = 'email'
      placeholder='joe@schmoe.com'
      error={{
        content: 'Please enter a valid email address',
        pointing: 'below'
      }}
      onChange ={(e) => setEmailid(e.target.value)}
    />
    <div>
    <Button positive
    onClick={addDatatoapi}
    >
        Enter Data
        </Button>

    </div>
  </Form>

</div>
  )
}

export default Create
