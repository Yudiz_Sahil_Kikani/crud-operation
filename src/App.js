import React from 'react'
import './App.css'
import Create from './components/create/Create'
import Read from './components/read/Read'
import Update from './components/update/Update'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

function App () {
  return (
    <div className="main">
      <div>
        <h1>Axios-Demo</h1><br/>
      </div>

    <Router>
      <Routes>
        <Route path='/' element={<Create/>} />
        <Route excat path='/read' element={<Read/>} />
        <Route path='/update' element={<Update/>} />
     </Routes>
    </Router>

    </div>
  )
}

export default App
